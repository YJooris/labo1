package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController // indicates that this class is a web @Controller, so Spring considers it when handling incoming web requests.
@SpringBootApplication    // both @SpringBootApplication and @EnableAutoConfiguration seem to do the same thing
//@EnableAutoConfiguration    // both @SpringBootApplication and @EnableAutoConfiguration seem to do the same thing
public class DemoApplication {
    @RequestMapping("/")    // provides “routing” information. It tells Spring that any HTTP request with the / path should be mapped to the home method.
    String home() {
        return "Hello World!";
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
}
